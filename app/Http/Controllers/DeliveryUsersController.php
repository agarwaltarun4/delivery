<?php


namespace App\Http\Controllers;


use App\DeliveryUserHelper;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class DeliveryUsersController extends Controller
{

    public function create(Request $request)
    {
        $name = $request->get('name');
        $deliveryUserId = DeliveryUserHelper::create($name);
        return JsonResponse::create(['deliveryUserId' => $deliveryUserId]);
    }

    public function assign(Request $request)
    {
        $orderId = $request->get('orderId');
        $deliveryUserId = $request->get('deliveryUserId');
        $response['status'] = 'FAILED';
        if (DeliveryUserHelper::checkIfNotActive($deliveryUserId)) {
            $response['msg'] = 'Delivery Boy Not Active';
            return JsonResponse::create($response)->send();
        }
        if (DeliveryUserHelper::assign($orderId, $deliveryUserId)) {
            $response['status'] = 'ASSIGNED';
            return JsonResponse::create($response)->send();
        } else {
            return JsonResponse::create($response)->send();
        }
    }

    public function state(Request $request)
    {
        $deliveryUserId = $request->get('deliveryUserId');
        return JsonResponse::create(DeliveryUserHelper::getState($deliveryUserId))->send();
    }
}
