<?php


namespace App;


use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class DeliveryUserHelper
{

    public static function checkIfNotActive($id)
    {
        $deliveryUsers = DeliveryUsers::where('id', '=', $id)
            ->where('curr_order_id', '=', 0)
            ->get()
            ->toArray();
        return empty($deliveryUsers);
    }

    public static function assign($orderId, $deliveryUserId)
    {
        DB::beginTransaction();
        try {
            $deliveryUser = DeliveryUsers::find($deliveryUserId);
            $deliveryUser->curr_order_id = $orderId;
            $deliveryUser->save();

            $order = Orders::find($orderId);
            $order->delivery_person_id = $deliveryUserId;
            $order->status = 'ACCEPTED';
            $order->save();
        } catch (\Exception $exception) {
            DB::rollBack();
            Log::error("Error in assigning partner " . $exception->getMessage());
            return false;
        } finally {
            DB::commit();
            return true;
        }
    }

    public static function getState($deliveryUserId)
    {
        $deliveryUsers = DeliveryUsers::select('status', 'name', 'est_delivery_time', 'orders.id as order_id')
            ->leftJoin('orders', 'orders.id', '=', 'curr_order_id')
            ->where('delivery_users.id', '=', $deliveryUserId)
            ->get()
            ->toArray();

        foreach ($deliveryUsers as $idx => $deliveryUser) {
            $deliveryUsers[$idx]['state'] = empty($deliveryUser['order_id']) ? 'INACTIVE' : 'ACTIVE';
            if (!empty($deliveryUser['order_id'])) {
                $time_left = $deliveryUser['est_delivery_time'] - microtime(true);
                $time_status = $time_left > 0 ? ' LEFT' : ' DELAY';
                $deliveryUsers[$idx]['est_delivery_time'] =
                    date('r', $deliveryUser['est_delivery_time'] + 19800);
                $deliveryUsers[$idx]['time_left'] = round(ceil(abs($time_left / 60)), 1) . ' MINUTES' . $time_status;
            }
        }
        return array_filter($deliveryUsers[0] ?? []);
    }

    public static function create($name)
    {
        $deliveryUser = new DeliveryUsers();
        $deliveryUser->name = $name;
        $deliveryUser->save();
        return $deliveryUser->id;
    }
}
